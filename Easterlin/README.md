# Description of files.

This folder contains a number of R scripts used for chapter 6 of the thesis.
To recreate the analysis, the following steps need to be undertaken.
Always keep the working directory set to the folder containing this file

1. Run the empirical_plots.R script to create this plot. You will need to input HFD credentials.
2. Run the simulation at the points shown in ..data/Easterlin/designs/wave2/design.csv
3. Use 'process_data.R' to convert the simulation outputs to simpler input files. You will need to ensure the correct date is used.
4. Run the 'implausbility_1.R' file to fit emulators and locate a reduced, non-implausbile parameter sample.
5. Run 'opt_depth.R' to generate the optical depth plot for wave2.
6. Run 'implausiblity_2.R' to find and save the final, calibrated parameter set.
7. Run 'opt_depth.R' to generate the optical depth plot for wave3.
8. Run the simulation at the points in ..data/Easterlin/designs/calibrated/design.csv
9. Run the test_results.R script to generate the birth times series and asfr plots.
10. Run the process_md.R script in the master phd_Document directory using R CMD BATCH process_md.R
11. Create the thesis tex file from the thesisjh.3.0.tex using pdflatex and bibtex

