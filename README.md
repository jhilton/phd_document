This repository provides the files for conducting the analysis presented in Jason Hilton's PhD thesis.
It also gives the R Markdown files used to produce the thesis.

The simulation required for chapter 6 is provided in [another repository](https://bitbucket.org/jhilton/easterlinphd).


# Prequisites

- R statistical computing language, Version 3.3.2, although earlier versions should also be sufficient.
- The accompanying R library `emulatorsPHD`, which contains the functions needed for fitting emulators and carrying out analysis with them. First, install the devtools library (`install.packages("devtools")`). Next, execute the following command from within R: `install_bitbucket("jhilton/emulatorsPHD")`.

- A number of other libraries are needed:
  + ggplot2
  + dplyr
  + rstan
  + lhs
  + tidyr
  + magrittr
  + tibble
  + ggally
  + ggmcmc
  + pander
  + rmarkdown
  + knitr
  + RSQLite
  
- Python is required to run the simulation in Chapter 6. Python 3 was used, but version 2.7 may also work.
The anaconda python distribution is recommended, as it includes many of the standard libraries.
- [Pandoc](http://pandoc.org/). This is a document conversion application that allows you to convert markdown to tex files. A version is included with Rstudio, but I have used a stand-alone installation, as this allow me to easily use the pandoc-eqnos, pandoc-tablenos and pandoc-fignos filters that provide for easy numbering of tables, figures and equations. These can be installed using python by typing pip install pandoc-**nos, where the stars are either table, fig or eq. 

# Description of files
The tex files are provided at the top level of the repository. Each chapter has a folder, which contains the relevant R markdown file and R analysis files where applicable. In addition, a data folder gives data for each chapter, and the a number of figure subdirectories are also present. 

# Data and static figures

The relevant data for running analysis and creating the various plots is available on dropbox [here](https://www.dropbox.com/sh/taokacc2x45ntya/AACeMf32ihBil9Caagw6qT4wa?dl=0).
All sub-folders available here should be copied into the top level of this repository in order to run the analysis.

# Creating the thesis document
Running the command `R CMD BATCH process_md.R` should create all the relevant tex files, assuming all the relevant thesis documents have been downloaded from dropbox. This will run the relevant R code. For the Analysis and Easterlin chapter, this fits emulators to simulation outputs and conducts the relevant analyses. For the calibration chapter, at present this only converts static output data to plots. After the tex files have been created, the thesis pdf itself can be created by running pdflatex and bibtex in the usual way on the document thesis3.0.tex.